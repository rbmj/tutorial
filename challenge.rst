=====================
Challenge Disassembly
=====================


===========
Walkthrough
===========

Starting off
------------

Just like in the first tutorial, we're going to cheat and look for a
recv().

.. code-block:: objdump                                                 
                                                                         
    8048d34:    8d 95 4c fb ff ff       lea    edx,[ebp-0x4b4]            
    8048d3a:    89 54 24 04             mov    DWORD PTR [esp+0x4],edx   
    8048d3e:    89 04 24                mov    DWORD PTR [esp],eax        
    8048d41:    e8 1a fa ff ff          call   8048760 <recv@plt>         

Scrolling up, you should see this is part of the function q_generate().
This looks like our connection-handling function.  So, without further ado,
let's get to decompiling this function.

The function begins with the following:

+---------------------------------------------------------+---------------+
| .. code-block:: objdump                                 | +-----------+ |
|                                                         | | | argN    | |
|    08048cf7 <q_generate>:                               | +-----------+ |
|     8048cf7:  55                      push   ebp        | | | ...     | |
|     8048cf8:  89 e5                   mov    ebp,esp    | +-----------+ |
|     8048cfa:  81 ec c8 04 00 00       sub    esp,0x4c8  | | | arg0    | |
|                                                         | +-----------+ |
|                                                         | | | ret adr | |
|                                                         | +-----------+ |
|                                                         | | | fp      | |
|                                                         | +-----------+ |
|                                                         | | | ...     | |
|                                                         | | | 0x4c8   | |
|                                                         | | | bytes   | |
|                                                         | | | ...     | |
|                                                         | +-----------+ |
+---------------------------------------------------------+---------------+

All the above does is save the old frame pointer, set **ebp** to our new
stack frame, and reserve 0x4c8 bytes of space on the stack.  The next couple
of lines is similarly simple:

+-----------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                               | +-----------+ |
|                                                                       | | | argN    | |
|                                                                       | +-----------+ |
|     8048d00:  c7 45 f4 00 00 00 00    mov    DWORD PTR [ebp-0xc],0x0  | | | ...     | |
|     8048d07:  c7 45 f0 00 00 00 00    mov    DWORD PTR [ebp-0x10],0x0 | +-----------+ |
|                                                                       | | | arg0    | |
|                                                                       | +-----------+ |
|                                                                       | | | ret adr | |
|                                                                       | +-----------+ |
|                                                                       | | | fp      | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | 0x4b8   | |
|                                                                       | | | bytes   | |
|                                                                       | | | ...     | |
|                                                                       | +-----------+ |
+-----------------------------------------------------------------------+---------------+

This just zeros out two dwords on the stack.  We'll call those dwords a
and b.  Now we'll get a little bit more complex:

+-----------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                               | +-----------+ |
|                                                                       | | | argN    | |
|                                                                       | +-----------+ |
|    8048d0e:   c7 44 24 04 01 00 00    mov    DWORD PTR [esp+0x4],0x1  | | | ...     | |
|    8048d15:   00                                                      | +-----------+ |
|    8048d16:   8b 45 08                mov    eax,DWORD PTR [ebp+0x8]  | | | arg0    | |
|    8048d19:   89 04 24                mov    DWORD PTR [esp],eax      | +-----------+ |
|    8048d1c:   e8 f8 fe ff ff          call   8048c19 <sques>          | | | ret adr | |
|                                                                       | +-----------+ |
|                                                                       | | | fp      | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | 0x4b0     | |
|                                                                       | | | bytes   | |
|                                                                       | | | ...     | |
|                                                                       | +-----------+ |
|                                                                       | | | carg1   | |
|                                                                       | +-----------+ |
|                                                                       | | | carg0   | |
|                                                                       | +-----------+ |
+-----------------------------------------------------------------------+---------------+

Now, we set *carg1* = 1 and we set *carg0* = \*(**ebp** + 8), which is
just *arg0*.  We then call sques().  Well, in order to understand what is
going on, we need to know what sques() does.

Looking at sques()
------------------

Here's the first stack frame for sques():

+---------------------------------------------------------+---------------+
| .. code-block:: objdump                                 | +-----------+ |
|                                                         | | | arg1    | |
|    08048c19 <sques>:                                    | +-----------+ |
|     8048c19:  55                      push   ebp        | | | arg0    | |
|     8048c1a:  89 e5                   mov    ebp,esp    | +-----------+ |
|     8048c1c:  57                      push   edi        | | | ret adr | |
|     8048c1d:  83 ec 34                sub    esp,0x34   | +-----------+ |
|                                                         | | | fp      | |
|                                                         | +-----------+ |
|                                                         | | | sedi    | |
|                                                         | +-----------+ |
|                                                         | | | ...     | |
|                                                         | | | 0x34    | |
|                                                         | | | bytes   | |
|                                                         | | | ...     | |
|                                                         | +-----------+ |
+---------------------------------------------------------+---------------+

Note that since this function uses (also called *clobbers*) **edi**, it
saves this register on the stack to be restored later in *sedi* after the
stack frame but before the sques()'s local variables.  Also, since we know
this function gets called with two arguments from how it's called in
q_generate(), we can simplify the stack frame a little.  Now, let's see
what this function actually does:

+-----------------------------------------------------------------+---------------+
| .. code-block:: objdump                                         | +-----------+ |
|                                                                 | | | arg1    | |
|    8048c20:   8b 45 0c           mov    eax,DWORD PTR [ebp+0xc] | +-----------+ |
|    8048c23:   83 f8 01           cmp    eax,0x1                 | | | arg0    | |
|    8048c26:   74 0a              je     8048c32 <sques+0x19>    | +-----------+ |
|    8048c28:   83 f8 02           cmp    eax,0x2                 | | | ret adr | |
|    8048c2b:   74 7d              je     8048caa <sques+0x91>    | +-----------+ |
|    8048c2d:   e9 bf 00 00 00     jmp    8048cf1 <sques+0xd8>    | | | fp      | |
|                                                                 | +-----------+ |
|                                                                 | | | sedi    | |
|                                                                 | +-----------+ |
|                                                                 | | | ...     | |
|                                                                 | | | 0x34    | |
|                                                                 | | | bytes   | |
|                                                                 | | | ...     | |
|                                                                 | +-----------+ |
+-----------------------------------------------------------------+---------------+

This is easiest to understanad by looking at the equivalent pseudo-c:

.. code-block:: c

   register eax;
   
   T sques(int arg0, int arg1) {
      eax = arg1;
      if (eax == 1) goto lone;
      if (eax == 2) goto ltwo;
      goto lthree;
      lone:
         //foo
      ltwo:
         //bar
      lthree:
         //baz
   }

We can't yet replace the labels with if-else clauses as we don't know
if they will fall through or not.  We could write a switch statement but
that wouldn't give us much more.  So, we have to look at more code.  If
you look at the addresses in the left column, you'll see that this starts
the lone: section.  Here it is:

+------------------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                                      | +-----------+ |
|                                                                              | | | arg1    | |
|    8048c32:   e8 f6 fe ff ff          call   8048b2d <getrand>               | +-----------+ |
|    8048c37:   89 45 f4                mov    DWORD PTR [ebp-0xc],eax         | | | arg0    | |
|    8048c3a:   8b 45 f4                mov    eax,DWORD PTR [ebp-0xc]         | +-----------+ |
|    8048c3d:   8b 04 85 70 b0 04 08    mov    eax,DWORD PTR [eax*4+0x804b070] | | | ret adr | |
|                                                                              | +-----------+ |
|                                                                              | | | fp      | |
|                                                                              | +-----------+ |
|                                                                              | | | sedi    | |
|                                                                              | +-----------+ |
|                                                                              | | | 4 bytes | |
|                                                                              | +-----------+ |
|                                                                              | | | rnum    | |
|                                                                              | +-----------+ |
|                                                                              | | | ...     | |
|                                                                              | | 0x2c      | |
|                                                                              | | | bytes   | |
|                                                                              | | | ...     | |
|                                                                              | +-----------+ |
+------------------------------------------------------------------------------+---------------+

A few interesting things about this snippet.  Now, we call the function
getrand().  Normally we'd have to figure out what that function actually
does.  However, since they were kind enough to leave us debugging information,
and because they used a good function name, it's obvious what getrand() does.
It gets a random number.  We'll store this in *rnum*.  We don't really care
about how it gets a random number for now - if and when we do, we can go over
and look at the disassembly for that function.  But for now, this description
is good enough.  The second thing is that the compiler actually inserts an
extraneous statement.  It sets *rnum* to **eax**, and then sets **eax** to
*rnum*.  Now, we can see that this is clearly unnecessary.  However, a
compiler that is not optimizing will often leave in strange statements like
this.  Finally, the last line uses eax as an offset into memory, probably a
table of some kind.  Let's take a look at what happens to be at 0x804b070.

We can find the following in challenge1.data:

.. code-block:: objdump

   Contents of section .data:
    804b068 00000000 00000000 808f0408 e48f0408  ................
    804b078 18900408 64900408 98900408 e4900408  ....d...........
    804b088 44910408 4f910408 68910408 6d910408  D...O...h...m...
    804b098 74910408 7b910408                    t...{...      
 
The first column is the address.  From this, it is clear that the address
we are looking for is in the first row, third column of data (so fourth
column of the file): 808f0408.  This looks like a pointer.  So, *rnum*
is being used as an index into an array of pointers.  Keeping in mind
that this is little-endian, we can see what this points to.  Looking in
challenge1.data again for address 08048f80:

.. code-block:: objdump

    Contents of section .rodata:
     8048f78 03000000 01000200 57617247 616d6573  ........WarGames
     8048f88 3a205468 65206c61 756e6368 20636f64  : The launch cod
     8048f98 65207468 6174204a 6f736875 61202266  e that Joshua "f
     8048fa8 69677572 6573206f 75742220 666f7220  igures out" for 
     8048fb8 68696d73 656c660a 61742074 68652065  himself.at the e
     8048fc8 6e64206f 66207468 65206d6f 76696520  nd of the movie 
     8048fd8 77617320 5f5f5f5f 5f5f0a00 57617247  was ______..WarG

This is located in the third column, first row of data.  If you look at
the ASCII equivalent, it is clear that this points to a string.  So our
data type is const char*[].  The equivalent C for this array is:

.. code-block:: c

    const char * questions[] = {
        "WarGames: The launch code that Joshua "figures out" for himself\n"
            "at the end of the movie was ______\n",
        "WarGames: The computer used by David was an ______\n",
        "WarGames: The computer that constantly plays wargames codenamed is _____\n",
        "WarGames: David was playing ______ at the arcade\n",
        "WarGames: The password David used to access the school computers was _____\n",
        "WarGames: ______ was the game Joshua needed to play to learn\n"
            "\"not to play is the best option\".\n"
    };

So, looking back up at the last assembly snippet, **eax** will hold the
value of *questions* [ *rnum* ].  Moving forward!

+------------------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                                      | +-----------+ |
|                                                                              | | | arg1    | |
|    8048c44:   c7 45 e4 ff ff ff ff    mov    DWORD PTR [ebp-0x1c],0xffffffff | +-----------+ |
|    8048c4b:   89 c2                   mov    edx,eax                         | | | arg0    | |
|    8048c4d:   b8 00 00 00 00          mov    eax,0x0                         | +-----------+ |
|    8048c52:   8b 4d e4                mov    ecx,DWORD PTR [ebp-0x1c]        | | | ret adr | |
|    8048c55:   89 d7                   mov    edi,edx                         | +-----------+ |
|    8048c57:   f2 ae                   repnz scas al,BYTE PTR es:[edi]        | | | fp      | |
|    8048c59:   89 c8                   mov    eax,ecx                         | +-----------+ |
|    8048c5b:   f7 d0                   not    eax                             | | | sedi    | |
|    8048c5d:   8d 48 ff                lea    ecx,[eax-0x1]                   | +-----------+ |
|                                                                              | | | 4 bytes | |
|                                                                              | +-----------+ |
|                                                                              | | | rnum    | |
|                                                                              | +-----------+ |
|                                                                              | | | 4 bytes | |
|                                                                              | +-----------+ |
|                                                                              | | | -1      | |
|                                                                              | +-----------+ |
|                                                                              | | | ...     | |
|                                                                              | | | 0x24    | |
|                                                                              | | | bytes   | |
|                                                                              | | | ...     | |
|                                                                              | +-----------+ |
+------------------------------------------------------------------------------+---------------+

The first line places a -1 (or, 2^32 - 1 in unsigned notation) at the
location indicated in the stack diagram.  Next, we move the string (ptr)
that we got from the random index into the questions array into **edx**.
Then, we zero **eax** and move the -1 value into **ecx**.  It then moves
the pointer to the string into **edi**.  The rest of the lines are used
to get the length of the string and put it in ecx.

How it actually does this is complicated, so I'll put it in pseudo-c:

.. code-block:: c

   uint8_t * edx = "some string"; //line 2
   uint32_t eax = 0; //line 3
   uint8_t al = (uint8_t)eax; //al is the low byte of eax by definition
   uin32_t ecx = (uint32_t)-1; //0xFFFFFFFF, or max of uint32_t
   uint8_t * edi = edx; //line 5
   //now for line 6:
   while (ecx-- != 0) {
      if (*(edi++) == al) break;
   }
   eax = ecx; //line 7
   eax = ~eax; //line 8
   ecx = eax - 1; //line 9 - puts string length in ecx
   
Basically, the way it works is that **ecx** should never be 0 in the loop,
as the number is so huge (2^32 - 1).  Since **al** == 0, the break condition
is a null terminator for the string.  The loop will decrement the loop
n + 1 times, where n is the length of the string (as it decrements on every
test).  This gives a value for **ecx** of -1 - (n + 1) = -(n + 2).  Then,
when we invert it bitwise.  The signed arithmetic equivalent to a bitwise
inversion is negation and subtracting one (since the bitwise negation is
the inversion plus one).  This means that **eax** = (n + 2) - 1 = n + 1.
Then, we subtract one from eax and store this in **ecx**, which receives
just n, the length of the string.

Or, you could take it on faith that that line gets the length of the
string.  But it's kind of a cool hack, when you think about it...
   
Moving right along to more code, we have this function call:

+------------------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                                      | +-----------+ |
|                                                                              | | | arg1    | |
|    8048c60:   8b 45 f4                mov    eax,DWORD PTR [ebp-0xc]         | +-----------+ |
|    8048c63:   8b 14 85 70 b0 04 08    mov    edx,DWORD PTR [eax*4+0x804b070] | | | arg0    | |
|    8048c6a:   8b 45 08                mov    eax,DWORD PTR [ebp+0x8]         | +-----------+ |
|    8048c6d:   c7 44 24 0c 00 00 00    mov    DWORD PTR [esp+0xc],0x0         | | | ret adr | |
|    8048c74:   00                                                             | +-----------+ |
|    8048c75:   89 4c 24 08             mov    DWORD PTR [esp+0x8],ecx         | | | fp      | |
|    8048c79:   89 54 24 04             mov    DWORD PTR [esp+0x4],edx         | +-----------+ |
|    8048c7d:   89 04 24                mov    DWORD PTR [esp],eax             | | | sedi    | |
|    8048c80:   e8 fb fa ff ff          call   8048780 <send@plt>              | +-----------+ |
|                                                                              | | | 4 bytes | |
|                                                                              | +-----------+ |
|                                                                              | | | rnum    | |
|                                                                              | +-----------+ |
|                                                                              | | | 4 bytes | |
|                                                                              | +-----------+ |
|                                                                              | | | -1      | |
|                                                                              | +-----------+ |
|                                                                              | | | ...     | |
|                                                                              | | | 0x14    | |
|                                                                              | | | bytes   | |
|                                                                              | | | ...     | |
|                                                                              | +-----------+ |
|                                                                              | | | 0x0     | |
|                                                                              | +-----------+ |
|                                                                              | | | len     | |
|                                                                              | +-----------+ |
|                                                                              | | | str     | |
|                                                                              | +-----------+ |
|                                                                              | | | arg0    | |
|                                                                              | +-----------+ |
+------------------------------------------------------------------------------+---------------+

So, what we do is we put some items at the top of the stack as arguments to the function call,
which looks like send(*arg0*, *str*, *len*, 0x0); which we can cross reference with the man
page for send and find that *arg0* is the file descriptor of the socket we want to send over.
We know that *arg0* is also the first argument to sques(), which, if you look at the stack
frames for the previous function, is also the first argument to q_generate().  Right now, we can
replace both *arg0* for sques() and *arg0* for q_generate() with a more meaningful name.  I'll
use *sock*.

The other thing we see is some more extraneous instructions emitted by the compiler in lines
1 and 2.  Moving right along:

+------------------------------------------------------------------------------+-----------------+
| .. code-block:: objdump                                                      | +-------------+ |
|                                                                              | | | arg1      | |
|    8048c85:   8b 45 08                mov    eax,DWORD PTR [ebp+0x8]         | +-------------+ |
|    8048c88:   c7 44 24 0c 00 00 00    mov    DWORD PTR [esp+0xc],0x0         | | | sock      | |
|    8048c8f:   00                                                             | +-------------+ |
|    8048c90:   c7 44 24 08 08 00 00    mov    DWORD PTR [esp+0x8],0x8         | | | ret adr   | |
|    8048c97:   00                                                             | +-------------+ |
|    8048c98:   c7 44 24 04 10 92 04    mov    DWORD PTR [esp+0x4],0x8049210   | | | fp        | |
|    8048c9f:   08                                                             | +-------------+ |
|    8048ca0:   89 04 24                mov    DWORD PTR [esp],eax             | | | sedi      | |
|    8048ca3:   e8 d8 fa ff ff          call   8048780 <send@plt>              | +-------------+ |
|    8048ca8:   eb 47                   jmp    8048cf1 <sques+0xd8>            | | | 4 bytes   | |
|                                                                              | +-------------+ |
|                                                                              | | | rnum      | |
|                                                                              | +-------------+ |
|                                                                              | | | 4 bytes   | |
|                                                                              | +-------------+ |
|                                                                              | | | -1        | |
|                                                                              | +-------------+ |
|                                                                              | | | ...       | |
|                                                                              | | | 0x14      | |
|                                                                              | | | bytes     | |
|                                                                              | | | ...       | |
|                                                                              | +-------------+ |
|                                                                              | | | 0x0       | |
|                                                                              | +-------------+ |
|                                                                              | | | 0x8       | |
|                                                                              | +-------------+ |
|                                                                              | | | 0x8049210 | |
|                                                                              | +-------------+ |
|                                                                              | | | sock      | |
|                                                                              | +-------------+ |
+------------------------------------------------------------------------------+-----------------+
 
More extra instructions - we don't need to put *sock* at the top of the stack again - it was
already there!  Other than that, basically we're just sending a constant string.  Look up this
address in challenge1.data and you get the string "Answer: ".  Since the length of this string
is 0x08, this checks out.
 
We then jump to 8048cf1, which if you look above is the same address as the third label
near the top of this function.  We also see that the next instruction is at address 8048caa,
so we're now in the second branch of execution.  Let's look inside here.  Keep in mind that
we are using the stack frame from before we analyzed the disassembly of the first branch, as
if we are in the second branch of execution the first branch has not executed.  We can be
sure of this because there is no fall-through; the first branch ends with an unconditional
jump to the third label.  So, when it's all said and done, here's the disassembly of the
second branch.  As there isn't any stored computation on this branch, I'm not going to
draw out the stack frame:

.. code-block:: objdump

     8048caa:   8b 45 08                mov    eax,DWORD PTR [ebp+0x8]
     8048cad:   c7 44 24 0c 00 00 00    mov    DWORD PTR [esp+0xc],0x0
     8048cb4:   00 
     8048cb5:   c7 44 24 08 24 00 00    mov    DWORD PTR [esp+0x8],0x24
     8048cbc:   00 
     8048cbd:   c7 44 24 04 1c 92 04    mov    DWORD PTR [esp+0x4],0x804921c
     8048cc4:   08 
     8048cc5:   89 04 24                mov    DWORD PTR [esp],eax
     8048cc8:   e8 b3 fa ff ff          call   8048780 <send@plt>
     8048ccd:   8b 45 08                mov    eax,DWORD PTR [ebp+0x8]
     8048cd0:   c7 44 24 0c 00 00 00    mov    DWORD PTR [esp+0xc],0x0
     8048cd7:   00 
     8048cd8:   c7 44 24 08 08 00 00    mov    DWORD PTR [esp+0x8],0x8
     8048cdf:   00 
     8048ce0:   c7 44 24 04 10 92 04    mov    DWORD PTR [esp+0x4],0x8049210
     8048ce7:   08 
     8048ce8:   89 04 24                mov    DWORD PTR [esp],eax
     8048ceb:   e8 90 fa ff ff          call   8048780 <send@plt>
     8048cf0:   90                      nop
   
As you (hopefully) can see, this is equivalent to send(*sock*, 0x804921c, 0x24, 0)
followed by send(*sock*, 0x8049210, 0x8, 0).  The two strings to be sent over
*sock* are:

.. code-block:: c

    const char * try_again = "That's too bad. Try one more time!\n\n";
    const char * answer_prompt = "Answer: "; //we've seen this one before

Then, there's just a nop, and we fall through to 8048cf1, which is the third
label.  It also appears to us that the second argument is the attempt (either
first or second), so we can go ahead and change that variable name.

So, going back to the control flow pseudo-c:

.. code-block:: c

   register eax;
   
   T sques(int sock, int attempt) {
      eax = attempt;
      if (eax == 1) goto lone;
      if (eax == 2) goto ltwo;
      goto lthree;
      lone:
         //foo
         goto lthree; //the unconditional jump at the end of the first branch
      ltwo:
         //bar
      lthree:
         //baz
   }

We now how the control logic of the program works, so we can replace these
labels with the if-else sequence:

.. code-block:: c

   register eax;
   
   T sques(int sock, int attempt) {
      eax = attempt;
      if (eax == 1) {
         //foo
      }
      else if (eax == 2) {
         //bar
      }
      //baz
   }

So we've already analyzed foo and bar, so let's look at baz:

.. code-block:: objdump

     8048cf1:   83 c4 34                add    esp,0x34
     8048cf4:   5f                      pop    edi
     8048cf5:   5d                      pop    ebp
     8048cf6:   c3                      ret    

All this does is it just moves **esp** back up to point at the stack frame.
Then it just restores **edi** and **ebp** from the values that were pushed
onto the stack, and returns.  What is the return value of the function?
Well, that is stuck in **eax**, and since there's nothing meaningful left
in there, then we can conclude that the function doesn't return anything.
Therefore, we'll conclude that the return type is void.  Now, let's write
this function in C:

.. code-block:: c
   
   void sques(int sock, int attempt) {
      if (attempt == 1) {
         unsigned rnum = getrand();
         send(sock, questions[rnum], strlen(questions[rnum]), 0);
         send(sock, answer_prompt, strlen(answer_prompt), 0);
      }
      else if (attempt == 2) {
         send(sock, try_again, strlen(try_again), 0);
         send(sock, answer_prompt, strlen(answer_prompt), 0);
      }
   }
   
That was quite a bit of assembly code for so little C.  If you didn't
know already, now you know why people only code in assembly when they
absolutely have to (for the most part).

I can say with a decent amount of confidence that this is how the source
code looked when they compiled the code.  Why are there no references to
strlen() in the assembly?  Remember that GCC has builtin functions, so
it can inline them (with the rep loop), or, if the length is known at
compile time, replace them with a constant.

Back to q_generate()
--------------------

So, now that we know what sques() does, i.e. send a question, we can
go back to q_generate().  On to the next stack frame:

+-----------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                               | +-----------+ |
|                                                                       | | | argN    | |
|                                                                       | +-----------+ |
|    8048d21:   8b 45 08                mov    eax,DWORD PTR [ebp+0x8]  | | | ...     | |
|    8048d24:   c7 44 24 0c 00 00 00    mov    DWORD PTR [esp+0xc],0x0  | +-----------+ |
|    8048d2b:   00                                                      | | | sock    | |
|    8048d2c:   c7 44 24 08 7c 00 00    mov    DWORD PTR [esp+0x8],0x7c | +-----------+ |
|    8048d34:   8d 95 4c fb ff ff       lea    edx,[ebp-0x4b4]          | | | ret adr | |
|    8048d3a:   89 54 24 04             mov    DWORD PTR [esp+0x4],edx  | +-----------+ |
|    8048d3e:   89 04 24                mov    DWORD PTR [esp],eax      | | | fp      | |
|    8048d41:   e8 1a fa ff ff          call   8048760 <recv@plt>       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | X bytes | |
|                                                                       | | | ...     | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | buf[]   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x7c    | |
|                                                                       | +-----------+ |
|                                                                       | | | &buf    | |
|                                                                       | +-----------+ |
|                                                                       | | | sock    | |
|                                                                       | +-----------+ |
+-----------------------------------------------------------------------+---------------+

In this section, we put arguments on the stack for a call to recv().  We
see that these arguments are *sock*, a location in memory (*buf* = 
**ebp** - 0x4b4), 0x7c, and 0.  According to the documentation for recv(),
this will receive up to 0x7c bytes into *buf* over *sock*.  The actual
number of bytes read will be returned in **eax**.  Onto the next snippet:

+-----------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                               | +-----------+ |
|                                                                       | | | argN    | |
|                                                                       | +-----------+ |
|    8048d46:   89 45 f0                mov    DWORD PTR [ebp-0x10],eax | | | ...     | |
|    8048d4c:   89 44 24 04             mov    DWORD PTR [esp+0x4],eax  | +-----------+ |
|    8048d49:   8b 45 f0                mov    eax,DWORD PTR [ebp-0x10] | | | sock    | |
|    8048d4c:   89 44 24 04             mov    DWORD PTR [esp+0x4],eax  | +-----------+ |
|    8048d50:   8d 85 4c fb ff ff       lea    eax,[ebp-0x4b4]          | | | ret adr | |
|    8048d56:   89 04 24                mov    DWORD PTR [esp],eax      | +-----------+ |
|    8048d59:   e8 aa fe ff ff          call   8048c08 <addn>           | | | fp      | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | read    | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | X bytes | |
|                                                                       | | | ...     | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | buf[]   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x7c    | |
|                                                                       | +-----------+ |
|                                                                       | | | read    | |
|                                                                       | +-----------+ |
|                                                                       | | | &buf    | |
|                                                                       | +-----------+ |
+-----------------------------------------------------------------------+---------------+

So, after some wasted operations, we call addn(& *buf*, *read*).  And
off to analyze another function.

Diving into addn()
------------------

After what's gone before, this one will be really easy.  I'll just show
the disassembly - no need for stack diagrams here:

.. code-block:: objdump

    08048c08 <addn>:
     8048c08:   55                      push   ebp
     8048c09:   89 e5                   mov    ebp,esp
     8048c0b:   8b 45 0c                mov    eax,DWORD PTR [ebp+0xc]
     8048c0e:   83 e8 01                sub    eax,0x1
     8048c11:   03 45 08                add    eax,DWORD PTR [ebp+0x8]
     8048c14:   c6 00 00                mov    BYTE PTR [eax],0x0
     8048c17:   5d                      pop    ebp
     8048c18:   c3                      ret   
 
This translates to:

.. code-block:: c

    void addn(uint8_t * buf, uint32_t read) {
        buf[read-1] = '\0';
    }

And we're done.  It's that simple.  Note that, in this case,
*buf*[*read* - 1] will probably be '\n', so this will just delete the
trailing newline and make it a null-terminating c-string.
 
To q_generate() once more
-------------------------

So, once we have added a null terminator to *buf*, we can move on:

+-----------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                               | +-----------+ |
|                                                                       | | | argN    | |
|                                                                       | +-----------+ |
|    8048d5e:   8d 85 4c fb ff ff       lea    eax,[ebp-0x4b4]          | | | ...     | |
|    8048d64:   89 04 24                mov    DWORD PTR [esp],eax      | +-----------+ |
|    8048d67:   e8 02 fe ff ff          call   8048b6e <v>              | | | sock    | |
|                                                                       | +-----------+ |
|                                                                       | | | ret adr | |
|                                                                       | +-----------+ |
|                                                                       | | | fp      | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | read    | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | X bytes | |
|                                                                       | | | ...     | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | buf[]   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x7c    | |
|                                                                       | +-----------+ |
|                                                                       | | | read    | |
|                                                                       | +-----------+ |
|                                                                       | | | &buf    | |
|                                                                       | +-----------+ |
+-----------------------------------------------------------------------+---------------+

Just kidding.  We now have to look at v()...
 
v(): Not the smartest function
------------------------------

Remember how the sques() function didn't return anything? Because of this,
v(), which will turn out to be the validator function, will have to cheat.

Let's see how the assembly looks.  Our first stack frame:

+--------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                            | +-----------+ |
|                                                                    | | | buf     | |
|    08048b6e <v>:                                                   | +-----------+ |
|     8048b6e:  55                    push   ebp                     | | | ret adr | |
|     8048b6f:  89 e5                 mov    ebp,esp                 | +-----------+ |
|     8048b71:  83 ec 28              sub    esp,0x28                | | | fp      | |
|     8048b74:  c7 45 f4 00 00 00 00  mov    DWORD PTR [ebp-0xc],0x0 | +-----------+ |
|     8048b7b:  eb 28                 jmp    8048ba5 <v+0x37>        | | | 0x08    | |
|                                                                    | | | bytes   | |
|                                                                    | +-----------+ |
|                                                                    | | | 0       | |
|                                                                    | +-----------+ |
|                                                                    | | | ...     | |
|                                                                    | | | 0x1c    | |
|                                                                    | | | bytes   | |
|                                                                    | | | ...     | |
|                                                                    | +-----------+ |
+--------------------------------------------------------------------+---------------+

Not too much - we set up the stack frame and then do an unconditional
jump.  The location we jump to then has the following:

+--------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                            | +-----------+ |
|                                                                    | | | buf     | |
|    8048ba5:   83 7d f4 05       cmp    DWORD PTR [ebp-0xc],0x5     | +-----------+ |
|    8048ba9:   76 d2             jbe    8048b7d <v+0xf>             | | | ret adr | |
|                                                                    | +-----------+ |
|                                                                    | | | fp      | |
|                                                                    | +-----------+ |
|                                                                    | | | 0x08    | |
|                                                                    | | | bytes   | |
|                                                                    | +-----------+ |
|                                                                    | | | count   | |
|                                                                    | +-----------+ |
|                                                                    | | | ...     | |
|                                                                    | | | 0x1c    | |
|                                                                    | | | bytes   | |
|                                                                    | | | ...     | |
|                                                                    | +-----------+ |
+--------------------------------------------------------------------+---------------+

The **jbe** instruction is the unsigned "jump if less than" instruction,
so this looks like a "execute for *count* from 0 - 5" loop.  Here's the
loop body (everything between the jump and the condition):

+-----------------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                                     | +-----------+ |
|                                                                             | | | buf     | |
|    8048b7d:   8b 45 f4               mov    eax,DWORD PTR [ebp-0xc]         | +-----------+ |
|    8048b80:   8b 04 85 88 b0 04 08   mov    eax,DWORD PTR [eax*4+0x804b088] | | | ret adr | |
|    8048b87:   8b 55 08               mov    edx,DWORD PTR [ebp+0x8]         | +-----------+ |
|    8048b8a:   89 54 24 04            mov    DWORD PTR [esp+0x4],edx         | | | fp      | |
|    8048b8e:   89 04 24               mov    DWORD PTR [esp],eax             | +-----------+ |
|    8048b91:   e8 2a fb ff ff         call   80486c0 <strcasecmp@plt>        | | | 0x08    | |
|    8048b96:   85 c0                  test   eax,eax                         | | | bytes   | |
|    8048b98:   75 07                  jne    8048ba1 <v+0x33>                | +-----------+ |
|    8048b9a:   b8 01 00 00 00         mov    eax,0x1                         | | | count   | |
|    8048b9f:   eb 0f                  jmp    8048bb0 <v+0x42>                | +-----------+ |
|    8048ba1:   83 45 f4 01            add    DWORD PTR [ebp-0xc],0x1         | | | ...     | |
|                                                                             | | | 0x14    | |
|                                                                             | | | bytes   | |
|                                                                             | | | ...     | |
|                                                                             | +-----------+ |
|                                                                             | | | buf     | |
|                                                                             | +-----------+ |
|                                                                             | | | str     | |
|                                                                             | +-----------+ |
+-----------------------------------------------------------------------------+---------------+

The **test** and **jne** combination will jump if **eax** is not 0.  This will
be the case if strcasecmp() thinks that *buf* and *str* are unequal.  If
they are equal (and therefore no jump is made), the function will store 1
in **eax** and then jump to the end.  Otherwise, *count* will be incremented
and we'll fall through to the loop condition.
 
The last little bit of v():

.. code-block:: objdump

     8048bab:   b8 00 00 00 00          mov    eax,0x0
     8048bb0:   c9                      leave  
     8048bb1:   c3                      ret    
     
Note that the **jne** if the strings match will jump to the **leave**, so
**eax** will still be 1.  Otherwise, if we went through the loop without
finding a match, **eax** will be set to 0.  The funciton returns.

Note how, because it doesn't know what question was asked, v() simply
checks if *buf* is the answer to **any** of the questions.

So, the C code:

.. code-block:: c

    bool v(const char * str) {
        for (unsigned i = 0; i < sizeof(questions)/sizeof(questions[0]); ++i) {
            if (strcasecmp(str, questions[i]) == 0) {
                return true;
            }
        }
        return false;
    }

And we can (finally) return to q_generate() again!

And we're back (again)

q_generate(): One last time...
------------------------------

+-----------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                               | +-----------+ |
|                                                                       | | | argN    | |
|                                                                       | +-----------+ |
|    8048d6c:   89 45 f4               mov    DWORD PTR [ebp-0xc],eax   | | | ...     | |
|    8048d6f:   83 7d f4 01            cmp    DWORD PTR [ebp-0xc],0x1   | +-----------+ |
|    8048d73:   75 22                  jne    8048d97 <q_generate+0xa0> | | | sock    | |
|    8048d75:   8b 45 08               mov    eax,DWORD PTR [ebp+0x8]   | +-----------+ |
|    8048d78:   89 04 24               mov    DWORD PTR [esp],eax       | | | ret adr | |
|    8048d7b:   e8 32 fe ff ff         call   8048bb2 <win>             | +-----------+ |
|    8048d80:   8b 45 08               mov    eax,DWORD PTR [ebp+0x8]   | | | fp      | |
|    8048d83:   89 04 24               mov    DWORD PTR [esp],eax       | +-----------+ |
|    8048d86:   e8 e5 f9 ff ff         call   8048770 <close@plt>       | | | 0x08    | |
|    8048d8b:   c7 04 24 00 00 00 00   mov    DWORD PTR [esp],0x0       | | | bytes   | |
|    8048d92:   e8 f9 f8 ff ff         call   8048690 <exit@plt>        | +-----------+ |
|                                                                       | | | right   | |
|                                                                       | +-----------+ |
|                                                                       | | | read    | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | X bytes | |
|                                                                       | | | ...     | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | buf[]   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x7c    | |
|                                                                       | +-----------+ |
|                                                                       | | | read    | |
|                                                                       | +-----------+ |
|                                                                       | | | sock / 0| |
|                                                                       | +-----------+ |
+-----------------------------------------------------------------------+---------------+

This is another conditional branch.  In the first branch (if *read* is true),
We just call win( *sock*), close( *sock*), and then exit(0).  For now,
due to the description, we're just going to assume that win() outputs
some message.  We can disassemble it later, as it's pretty clear it does
not do much to change the meaning of the function.

Let's look at the other branch of execution (if v() returned false) now:


+-----------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                               | +-----------+ |
|                                                                       | | | argN    | |
|                                                                       | +-----------+ |
|    8048d97:   8b 45 08                mov    eax,DWORD PTR [ebp+0x8]  | | | ...     | |
|    8048d9a:   89 04 24                mov    DWORD PTR [esp],eax      | +-----------+ |
|    8048d9d:   e8 3b fe ff ff          call   8048bdd <lose>           | | | sock    | |
|    8048da2:   c7 44 24 04 02 00 00    mov    DWORD PTR [esp+0x4],0x2  | +-----------+ |
|    8048da9:   00                                                      | | | ret adr | |
|    8048daa:   8b 45 08                mov    eax,DWORD PTR [ebp+0x8]  | +-----------+ |
|    8048dad:   89 04 24                mov    DWORD PTR [esp],eax      | | | fp      | |
|    8048db0:   e8 64 fe ff ff          call   8048c19 <sques>          | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | right   | |
|                                                                       | +-----------+ |
|                                                                       | | | read    | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | X bytes | |
|                                                                       | | | ...     | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | buf[]   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x7c    | |
|                                                                       | +-----------+ |
|                                                                       | | | 2       | |
|                                                                       | +-----------+ |
|                                                                       | | | sock    | |
|                                                                       | +-----------+ |
+-----------------------------------------------------------------------+---------------+

Here, we call lose( *sock*) and sques(*sock*, 2).  Next:

+-----------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                               | +-----------+ |
|                                                                       | | | argN    | |
|                                                                       | +-----------+ |
|    8048db5:   8b 45 08               mov    eax,DWORD PTR [ebp+0x8]   | | | ...     | |
|    8048db8:   c7 44 24 0c 00 00 00   mov    DWORD PTR [esp+0xc],0x0   | +-----------+ |
|    8048dbf:   00                                                      | | | sock    | |
|    8048dc0:   c7 44 24 08 00 04 00   mov    DWORD PTR [esp+0x8],0x400 | +-----------+ |
|    8048da7:   00                                                      | | | ret adr | |
|    8048dc8:   8d 95 c8 fb ff ff      lea    edx,[ebp-0x438]           | +-----------+ |
|    8048dce:   89 54 24 04            mov    DWORD PTR [esp+0x4],edx   | | | fp      | |
|    8048dd2:   89 04 24               mov    DWORD PTR [esp],eax       | +-----------+ |
|    8048dd5:   e8 86 f9 ff ff         call   8048760 <recv@plt>        | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | right   | |
|                                                                       | +-----------+ |
|                                                                       | | | read    | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | 0x28 b  | |
|                                                                       | | | ...     | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | 0x400 b | |
|                                                                       | | | buf[]   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x400   | |
|                                                                       | +-----------+ |
|                                                                       | | | &buf    | |
|                                                                       | +-----------+ |
|                                                                       | | | sock    | |
|                                                                       | +-----------+ |
+-----------------------------------------------------------------------+---------------+

So now we read up to 0x400 bytes into *buf*.  We have room, so everything
checks out.  I'm going to go ahead and mark sizeof( *buf*) as 400.
Moving forward:

+-----------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                               | +-----------+ |
|                                                                       | | | argN    | |
|                                                                       | +-----------+ |
|    8048dda:   89 45 f0               mov    DWORD PTR [ebp-0x10],eax  | | | ...     | |
|    8048ddd:   8b 45 f0               mov    eax,DWORD PTR [ebp-0x10]  | +-----------+ |
|    8048de0:   89 44 24 04            mov    DWORD PTR [esp+0x4],eax   | | | sock    | |
|    8048de4:   8d 85 c8 fb ff ff      lea    eax,[ebp-0x438]           | +-----------+ |
|    8048dea:   89 04 24               mov    DWORD PTR [esp],eax       | | | ret adr | |
|    8048ded:   e8 16 fe ff ff         call   8048c08 <addn>            | +-----------+ |
|                                                                       | | | fp      | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | right   | |
|                                                                       | +-----------+ |
|                                                                       | | | read    | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | 0x28 b  | |
|                                                                       | | | ...     | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | 0x400 b | |
|                                                                       | | | buf[]   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x400   | |
|                                                                       | +-----------+ |
|                                                                       | | | &buf    | |
|                                                                       | +-----------+ |
|                                                                       | | | sock    | |
|                                                                       | +-----------+ |
+-----------------------------------------------------------------------+---------------+

This is also simple.  We now just set *read* to the **new** number of
bytes received, then call addn(& *buf*, *read*).  This is going quick
now...

+---------------------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                                         | +-----------+ |
|                                                                                 | | | argN    | |
|    8048df2:   8b 45 f0           mov    eax,DWORD PTR [ebp-0x10]                | +-----------+ |
|    8048df5:   c1 e8 02           shr    eax,0x2                                 | | | ...     | |
|    8048df8:   89 c1              mov    ecx,eax                                 | +-----------+ |
|    8048dfa:   8d 85 c8 fb ff ff  lea    eax,[ebp-0x438]                         | | | sock    | |
|    8048e00:   89 c6              mov    esi,eax                                 | +-----------+ |
|    8048e02:   8d 45 c8           lea    eax,[ebp-0x38]                          | | | ret adr | |
|    8048e05:   89 c7              mov    edi,eax                                 | +-----------+ |
|    8048e07:   f3 a5              rep movs DWORD PTR es:[edi],DWORD PTR ds:[esi] | | | fp      | |
|                                                                                 | +-----------+ |
|                                                                                 | | | 0x08    | |
|                                                                                 | | | bytes   | |
|                                                                                 | +-----------+ |
|                                                                                 | | | right   | |
|                                                                                 | +-----------+ |
|                                                                                 | | | read    | |
|                                                                                 | +-----------+ |
|                                                                                 | | | ...     | |
|                                                                                 | | | 0x28 b  | |
|                                                                                 | | | nbuf[]  | |
|                                                                                 | +-----------+ |
|                                                                                 | | | ...     | |
|                                                                                 | | | 0x400 b | |
|                                                                                 | | | buf[]   | |
|                                                                                 | +-----------+ |
|                                                                                 | | | 0x08    | |
|                                                                                 | | | bytes   | |
|                                                                                 | +-----------+ |
|                                                                                 | | | 0       | |
|                                                                                 | +-----------+ |
|                                                                                 | | | 0x400   | |
|                                                                                 | +-----------+ |
|                                                                                 | | | &buf    | |
|                                                                                 | +-----------+ |
|                                                                                 | | | sock    | |
|                                                                                 | +-----------+ |
+---------------------------------------------------------------------------------+---------------+

This is a very strange sequence.  What it does is right-shifts the number of
bytes read by two bits (i.e. divides by 4), then copies that many dwords
to & *nbuf*.  This means we are copying a buffer whose length can be up to
0x400 bytes into a buffer whose length is only 0x28 bytes.  This is an
almost comical buffer overflow vulnerability.  It's like they're asking
for it (oh wait, they are...).

Moving right along (almost there, I promise!):

+-----------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                               | +-----------+ |
|                                                                       | | | argN    | |
|                                                                       | +-----------+ |
|    8048e09:   8d 45 c8              lea    eax,[ebp-0x38]             | | | ...     | |
|    8048e0c:   89 04 24              mov    DWORD PTR [esp],eax        | +-----------+ |
|    8048e0f:   e8 5a fd ff ff        call   8048b6e <v>                | | | sock    | |
|    8048e14:   89 45 f4              mov    DWORD PTR [ebp-0xc],eax    | +-----------+ |
|    8048e17:   83 7d f4 01           cmp    DWORD PTR [ebp-0xc],0x1    | | | ret adr | |
|    8048e1b:   75 22                 jne    8048e3f <q_generate+0x148> | +-----------+ |
|                                                                       | | | fp      | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | right   | |
|                                                                       | +-----------+ |
|                                                                       | | | read    | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | 0x28 b  | |
|                                                                       | | | nbuf[]  | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | 0x400 b | |
|                                                                       | | | buf[]   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x400   | |
|                                                                       | +-----------+ |
|                                                                       | | | &buf    | |
|                                                                       | +-----------+ |
|                                                                       | | | &nbuf   | |
|                                                                       | +-----------+ |
+-----------------------------------------------------------------------+---------------+

Nothing too complex.  Just call v(), and then jump if v is false. Let's
finish it off now, shall we?

+-----------------------------------------------------------------------+---------------+
| .. code-block:: objdump                                               | +-----------+ |
|                                                                       | | | argN    | |
|    8048e1d:   8b 45 08                mov    eax,DWORD PTR [ebp+0x8]  | +-----------+ |
|    8048e20:   89 04 24                mov    DWORD PTR [esp],eax      | | | ...     | |
|    8048e23:   e8 8a fd ff ff          call   8048bb2 <win>            | +-----------+ |
|    8048e28:   8b 45 08                mov    eax,DWORD PTR [ebp+0x8]  | | | sock    | |
|    8048e2b:   89 04 24                mov    DWORD PTR [esp],eax      | +-----------+ |
|    8048e2e:   e8 3d f9 ff ff          call   8048770 <close@plt>      | | | ret adr | |
|    8048e33:   c7 04 24 01 00 00 00    mov    DWORD PTR [esp],0x1      | +-----------+ |
|    8048e3a:   e8 51 f8 ff ff          call   8048690 <exit@plt>       | | | fp      | |
|    8048e3f:   8b 45 08                mov    eax,DWORD PTR [ebp+0x8]  | +-----------+ |
|    8048e42:   89 04 24                mov    DWORD PTR [esp],eax      | | | 0x08    | |
|    8048e45:   e8 93 fd ff ff          call   8048bdd <lose>           | | | bytes   | |
|    8048e4a:   c9                      leave                           | +-----------+ |
|    8048e4b:   c3                      ret                             | | | right   | |
|                                                                       | +-----------+ |
|                                                                       | | | read    | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | 0x28 b  | |
|                                                                       | | | nbuf[]  | |
|                                                                       | +-----------+ |
|                                                                       | | | ...     | |
|                                                                       | | | 0x400 b | |
|                                                                       | | | buf[]   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x08    | |
|                                                                       | | | bytes   | |
|                                                                       | +-----------+ |
|                                                                       | | | 0       | |
|                                                                       | +-----------+ |
|                                                                       | | | 0x400   | |
|                                                                       | +-----------+ |
|                                                                       | | | &buf    | |
|                                                                       | +-----------+ |
|                                                                       | | | sock /1 | |
|                                                                       | +-----------+ |
+-----------------------------------------------------------------------+---------------+

So all we do here is call win(sock), close(sock) and exit(1).  If we jumped
earlier, then we just call lose(sock) and return.  And this function is done.

Since we never used anything above arg0, we can saftely say it only takes
one argument.  It doesn't look like it returns anything meaningful either.

Let's write it up (as best we can) in C:

.. code-block:: c

    void q_generate(int sock) {
        int unused1, unused2;
        bool right = false;
        unsigned read = 0;
        char nbuf[40];
        char buf[0x400];
        
        sques(sock, 1);
        read = recv(sock, buf, 0x7c, 0); //why 0x7c?
        addn(buf, read);
        right = v(buf);
        if (right) {
            win(sock);
            close(sock);
            exit(0);
        }
        lose(sock);
        sques(sock, 2);
        read = recv(sock, buf, sizeof(buf), 0);
        addn(buf, read);
        memcpy(nbuf, buf, sizeof(buf));
        right = v(buf);
        if (right) {
            win(sock);
            close(sock);
            exit(1);
        }
        lose(sock);
    }

Now for the hard part.
